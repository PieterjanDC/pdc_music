﻿using System.Collections.Generic;

namespace MusicDemons.Data.Dtos
{
    public class Subject
    {
        public int Id { get; set; }

        public List<Medium> Media { get; set; }
    }
}
