﻿using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicDemons.Data.Dtos
{
    public class Song : Subject
    {
        public string Title { get; set; }
        public DateTime Released { get; set; }
        public string Lyrics { get; set; }

        public string Text => Title;

        public List<Artist> Artists { get; set; }

        [JsonIgnore]
        public CompletionField TitleSuggest => new CompletionField { Input = new[] { Title } };
    }
}
