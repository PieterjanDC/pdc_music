﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicDemons.Data.Dtos
{
    public class User
    {
        public int? Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PictureUrl { get; set; }
    }
}
