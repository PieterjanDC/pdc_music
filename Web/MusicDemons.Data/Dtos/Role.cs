﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicDemons.Data.Dtos
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
