﻿using MusicDemons.Data.Enums;

namespace MusicDemons.Data.Dtos
{
    public class MediumType
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public ePlayerType PlayerType { get; set; }
    }
}
