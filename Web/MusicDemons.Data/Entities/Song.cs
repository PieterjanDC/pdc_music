﻿using MusicDemons.Data.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicDemons.Data.Entities
{
    internal class Song : Subject, ISoftDelete
    {
        public string Title { get; set; }
        public DateTime Released { get; set; }

        public List<ArtistSong> Artists { get; set; }
        public List<Lyrics> Lyrics { get; set; }
    }
}
