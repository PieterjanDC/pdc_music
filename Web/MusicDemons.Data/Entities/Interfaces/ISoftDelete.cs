﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicDemons.Data.Entities.Interfaces
{
    public interface ISoftDelete
    {
        User UserInsert { get; set; }
        User UserUpdate { get; set; }
        User UserDelete { get; set; }
    }
}
