﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicDemons.Data.Entities
{
    public class User : IdentityUser<int>
    {
        public string PictureUrl { get; set; }

        internal List<Lyrics> Lyrics { get; set; }
        internal List<Like> Likes { get; set; }
    }
}
