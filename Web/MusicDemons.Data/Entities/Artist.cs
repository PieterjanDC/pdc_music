﻿using MusicDemons.Data.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicDemons.Data.Entities
{
    internal class Artist : Subject, ISoftDelete
    {
        public string Name { get; set; }
        public int? YearStarted { get; set; }
        public int? YearQuit { get; set; }

        public List<ArtistPerson> Members { get; set; }
        public List<ArtistSong> Songs { get; set; }
    }
}
