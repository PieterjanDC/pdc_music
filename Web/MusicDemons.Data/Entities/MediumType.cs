﻿using MusicDemons.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MusicDemons.Data.Entities
{
    internal class MediumType : Interfaces.ISoftDelete
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Description { get; set; }
        public ePlayerType PlayerType { get; set; }

        public User UserInsert { get; set; }
        public User UserUpdate { get; set; }
        public User UserDelete { get; set; }
    }
}
