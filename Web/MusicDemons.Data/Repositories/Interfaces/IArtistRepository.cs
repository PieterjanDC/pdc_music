﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories.Interfaces
{
    public interface IArtistRepository
    {
        IEnumerable<Dtos.Artist> GetArtists(bool include_relations = false);
        Dtos.Artist GetArtist(int id, bool include_relations = false);
        Task<Dtos.Artist> InsertArtist(Dtos.Artist artist);
        Task<Dtos.Artist> UpdateArtist(Dtos.Artist artist);
        Task DeleteArtist(int artist_id);
        Task SaveChangesAsync();
    }
}
