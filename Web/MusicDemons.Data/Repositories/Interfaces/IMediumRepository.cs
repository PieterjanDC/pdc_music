﻿using MusicDemons.Data.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories.Interfaces
{
    public interface IMediumRepository
    {
        Task StoreMedia(Subject subject, IEnumerable<Medium> media);
    }
}
