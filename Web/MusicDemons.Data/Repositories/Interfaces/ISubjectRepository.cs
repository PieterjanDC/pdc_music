﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories.Interfaces
{
    public interface ISubjectRepository
    {
        Task<Tuple<int, int>> GetLikes(int subjectId);
        Task<bool?> DoesLike(int subjectId);
        Task Like(int subjectId, bool like);

        Task SaveChangesAsync();
    }

}
