﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories.Interfaces
{
    public interface IPersonRepository
    {
        IEnumerable<Dtos.Person> GetPeople(bool include_relations = false);
        Dtos.Person GetPerson(int id, bool include_relations = false);
        Task<Dtos.Person> InsertPerson(Dtos.Person person);
        Task<Dtos.Person> UpdatePerson(Dtos.Person person);
        Task DeletePerson(int person_id);
        Task SaveChangesAsync();
    }
}
