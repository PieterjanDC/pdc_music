﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories.Interfaces
{
    public interface ISongRepository
    {
        IEnumerable<Dtos.Song> GetSongs(bool include_relations = false);
        Dtos.Song GetSong(int id, bool include_relations = false);
        Task<Dtos.Song> InsertSong(Dtos.Song song);
        Task<Dtos.Song> UpdateSong(Dtos.Song song);
        Task DeleteSong(int song_id);
        Task SaveChangesAsync();
    }
}