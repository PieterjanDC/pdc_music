﻿using MusicDemons.Data.Helpers;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories.Interfaces
{
    public interface IAccountRepository
    {
        Task<Tuple<Dtos.User, string>> Register(Dtos.User user, string password);
        Task<LoginResult> LocalLogin(string email, string password, bool remember);
        Task<Dtos.User> GetCurrentUser(ClaimsPrincipal userProperty);
        Task Logout();
    }
}
