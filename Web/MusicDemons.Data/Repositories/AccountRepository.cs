﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MusicDemons.Data.Exceptions.Account;
using MusicDemons.Data.Helpers;
using MusicDemons.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private MusicDemonsContext musicdemons_context;
        private UserManager<Entities.User> user_manager;
        private SignInManager<Entities.User> signin_manager;
        private JwtIssuerOptions jwtIssuerOptions;
        public AccountRepository(MusicDemonsContext musicdemons_context, UserManager<Entities.User> user_manager, SignInManager<Entities.User> signin_manager, IOptions<JwtIssuerOptions> jwtIssuerOptions)
        {
            this.musicdemons_context = musicdemons_context;
            this.user_manager = user_manager;
            this.signin_manager = signin_manager;
            this.jwtIssuerOptions = jwtIssuerOptions.Value;
        }

        public async Task<Tuple<Dtos.User, string>> Register(Dtos.User user, string password)
        {
            try
            {
                var entity_user = ToEntity(user);
                var identity_result = await user_manager.CreateAsync(entity_user, password);
                if (identity_result.Succeeded)
                {
                    var dto_user = ToDto(entity_user);
                    var confirmation_token = await user_manager.GenerateEmailConfirmationTokenAsync(entity_user);
                    return new Tuple<Dtos.User, string>(dto_user, confirmation_token);
                }
                else
                {
                    throw new RegistrationException(identity_result.Errors);
                }
            }
            catch (RegistrationException RegistrationEx)
            {
                throw RegistrationEx;
            }
            catch (Exception ex)
            {
                throw new RegistrationException(ex);
            }
        }
        public async Task<LoginResult> LocalLogin(string email, string password, bool remember)
        {
            try
            {
                var user = await user_manager.FindByEmailAsync(email);
                if (user == null)
                    throw new LoginException();

                var result = await signin_manager.CheckPasswordSignInAsync(user, password, false);
                if (result.Succeeded)
                {
                    return new LoginResult
                    {
                        Status = true,
                        Platform = "local",
                        User = ToDto(user),
                        Token = CreateToken(user)
                    };
                }
                else
                {
                    throw new LoginException();
                }
            }
            catch (LoginException loginEx)
            {
                throw loginEx;
            }
            catch (Exception ex)
            {
                throw new LoginException(ex);
            }
        }
        public async Task<Data.Dtos.User> GetCurrentUser(ClaimsPrincipal userProperty)
        {
            var user = await user_manager.GetUserAsync(userProperty);
            return ToDto(user);
        }
        public async Task Logout()
        {
            await signin_manager.SignOutAsync();
        }


        #region Helper methods
        private string CreateToken(Entities.User user)
        {
            var token_descriptor = new SecurityTokenDescriptor
            {
                Issuer = jwtIssuerOptions.Issuer,
                IssuedAt = DateTime.UtcNow,
                Audience = jwtIssuerOptions.Audience,
                NotBefore = DateTime.UtcNow,
                Expires = DateTime.UtcNow.Add(jwtIssuerOptions.ValidFor),
                Subject = new ClaimsIdentity(new[]
                {
            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            new Claim(ClaimTypes.Name, user.UserName),
            new Claim(ClaimTypes.Email, user.Email)
        }),
                SigningCredentials = new Func<SigningCredentials>(() => {
                    var bytes = Encoding.UTF8.GetBytes(jwtIssuerOptions.Key);
                    var signing_key = new SymmetricSecurityKey(bytes);
                    var signing_credentials = new SigningCredentials(signing_key, SecurityAlgorithms.HmacSha256Signature);
                    return signing_credentials;
                }).Invoke()
            };

            var token_handler = new JwtSecurityTokenHandler();
            var token = token_handler.CreateToken(token_descriptor);
            var str_token = token_handler.WriteToken(token);

            return str_token;
        }
        #endregion
        #region Conversion methods
        internal static Entities.User ToEntity(Dtos.User user)
        {
            if (user == null) return null;
            return new Entities.User
            {
                Id = user.Id ?? 0,
                Email = user.Email,
                UserName = user.UserName,
                PictureUrl = user.PictureUrl
            };
        }
        internal static Dtos.User ToDto(Entities.User user)
        {
            if (user == null) return null;
            return new Dtos.User
            {
                Id = user.Id,
                Email = user.Email,
                UserName = user.UserName,
                PictureUrl = user.PictureUrl
            };
        }
        #endregion
    }
}
