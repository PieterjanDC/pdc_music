﻿using MusicDemons.Data.Dtos;
using MusicDemons.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories
{
    public class MediumRepository : IMediumRepository
    {
        private MusicDemonsContext musicdemons_context;
        public MediumRepository(MusicDemonsContext musicdemons_context)
        {
            this.musicdemons_context = musicdemons_context;
        }

        public async Task StoreMedia(Subject subject, IEnumerable<Medium> media)
        {
            // Remove the old media
            foreach (var medium in musicdemons_context.Media.Where(m => m.Subject.Id == subject.Id))
                musicdemons_context.Media.Remove(medium);

            // Get entity from database
            var entity_subject = await musicdemons_context.Subjects.FindAsync(subject.Id);
            foreach (var medium in media)
            {
                var entity_medium = new Entities.Medium
                {
                    Subject = entity_subject,
                    Type = await musicdemons_context.MediumTypes.FindAsync(medium.Type.Id),
                    Value = medium.Value
                };
                musicdemons_context.Add(entity_medium);
            }
        }

        internal static Medium ToDto(Entities.Medium medium, bool include_relations = false)
        {
            if (medium == null) return null;
            if (include_relations)
            {
                return new Medium
                {
                    Id = medium.Id,
                    Type = MediumTypeRepository.ToDto(medium.Type),
                    Value = medium.Value
                };
            }
            else
            {
                return new Medium
                {
                    Id = medium.Id,
                    Value = medium.Value
                };
            }
        }
        internal static Entities.Medium ToEntity(Medium medium, MusicDemonsContext musicdemons_context)
        {
            if (medium == null) return null;
            return new Entities.Medium
            {
                Id = medium.Id,
                Type = musicdemons_context.MediumTypes.Find(medium.Type.Id),
                Value = medium.Value
            };
        }
    }
}
