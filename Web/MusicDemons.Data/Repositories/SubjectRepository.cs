﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using MusicDemons.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories
{
    public class SubjectRepository : ISubjectRepository
    {
        private IHttpContextAccessor http_context;
        private MusicDemonsContext musicdemons_context;
        private UserManager<Entities.User> user_manager;
        public SubjectRepository(IHttpContextAccessor http_context, MusicDemonsContext musicdemons_context, UserManager<Entities.User> user_manager)
        {
            this.http_context = http_context;
            this.musicdemons_context = musicdemons_context;
            this.user_manager = user_manager;
        }

        public async Task<Tuple<int, int>> GetLikes(int subjectId)
        {
            var likes = musicdemons_context.Likes
                .Where(l => l.SubjectId == subjectId)
                .Where(l => l.DoesLike)
                .Count();
            var dislikes = musicdemons_context.Likes
                .Where(l => l.SubjectId == subjectId)
                .Where(l => !l.DoesLike)
                .Count();
            return new Tuple<int, int>(likes, dislikes);
        }

        public async Task<bool?> DoesLike(int subjectId)
        {
            var user = await user_manager.GetUserAsync(http_context.HttpContext.User);
            var user_id = user?.Id ?? 0;

            var like = musicdemons_context.Likes
                .Where(l => l.SubjectId == subjectId)
                .Where(l => l.UserId == user_id)
                .FirstOrDefault();

            if (like == null) return null;
            else return like.DoesLike;
        }

        public async Task Like(int subjectId, bool like)
        {
            var user = await user_manager.GetUserAsync(http_context.HttpContext.User);
            var subject_db = await musicdemons_context.FindAsync<Entities.Subject>(subjectId);

            var existing_like = musicdemons_context.Likes
                .Where(l => l.SubjectId == subjectId)
                .Where(l => l.UserId == user.Id)
                .FirstOrDefault();

            if (existing_like == null)
            {
                var new_like = new Entities.Like(subject_db, user, like);
                await musicdemons_context.AddAsync(new_like);
            }
            else
            {
                existing_like.DoesLike = like;
                musicdemons_context.Update(existing_like);
            }
        }

        public async Task SaveChangesAsync()
        {
            await musicdemons_context.SaveChangesAsync();
        }
    }
}
