﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MusicDemons.Data.Entities;
using MusicDemons.Data.Helpers;
using MusicDemons.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicDemons.Data.Repositories
{
    public class SongRepository : ISongRepository
    {
        private IHttpContextAccessor http_context;
        private MusicDemonsContext musicdemons_context;
        private UserManager<Entities.User> user_manager;
        private SongHelper song_helper;
        private Nest.IElasticClient elastic_client;
        public SongRepository(IHttpContextAccessor http_context, MusicDemonsContext musicdemons_context, UserManager<Entities.User> user_manager, SongHelper song_helper, Nest.IElasticClient elastic_client)
        {
            this.http_context = http_context;
            this.musicdemons_context = musicdemons_context;
            this.user_manager = user_manager;
            this.song_helper = song_helper;
            this.elastic_client = elastic_client;
        }

        public IEnumerable<Dtos.Song> GetSongs(bool include_relations = false)
        {
            if (include_relations)
            {
                var songs = musicdemons_context.Songs
                    .Include(song => song.Lyrics)
                    .Include(song => song.Artists)
                        .ThenInclude(@as => @as.Artist)
                    .Include(song => song.Media)
                    .Select(song => ToDto(song, true));
                return songs;
            }
            else
            {
                var songs = musicdemons_context.Songs
                    //.Include(song => song.Lyrics)
                    .Select(song => ToDto(song, false));
                return songs;
            }
        }

        public Dtos.Song GetSong(int id, bool include_relations = false)
        {
            if (include_relations)
            {
                var song = musicdemons_context.Songs
                    .Include(s => s.Lyrics)
                    .Include(s => s.Artists)
                        .ThenInclude(@as => @as.Artist)
                    .Include(s => s.Media)
                    .SingleOrDefault(s => s.Id == id);
                return ToDto(song, true);
            }
            else
            {
                var song = musicdemons_context.Songs
                    .Include(s => s.Lyrics)
                    .SingleOrDefault(s => s.Id == id);
                return ToDto(song, false);
            }
        }

        public async Task<Dtos.Song> InsertSong(Dtos.Song song)
        {
            // Get current user
            var user = await user_manager.GetUserAsync(http_context.HttpContext.User);

            // Convert to entity
            var entity_song = ToEntity(song, user, musicdemons_context);
            entity_song.UserInsert = user;

            // Add to database
            musicdemons_context.Songs.Add(entity_song);
            musicdemons_context.SaveChanges();

            // Index
            var new_person = ToDto(entity_song);
            var index_status = await elastic_client.IndexDocumentAsync(new_person);

            return new_person;
        }

        public async Task<Dtos.Song> UpdateSong(Dtos.Song song)
        {
            // Find existing song
            var song_entity = musicdemons_context.Songs.Include(s => s.Artists)
                .Include(s => s.Lyrics)
                .SingleOrDefault(s => s.Id == song.Id);

            // Set new properties
            song_entity.Title = song.Title;
            song_entity.Released = song.Released;

            // Add/update/delete artists
            IEnumerable<ArtistSong> to_add, to_remove, to_update;
            song_helper.CalculateUpdatedArtists(song_entity, song, musicdemons_context, out to_add, out to_update, out to_remove);
            foreach (var item in to_remove)
                musicdemons_context.Entry(item).State = EntityState.Deleted;
            foreach (var item in to_add)
                musicdemons_context.Entry(item).State = EntityState.Added;
            foreach (var item in to_update)
                musicdemons_context.Entry(item).State = EntityState.Modified;

            // Set UserUpdate
            var user = await user_manager.GetUserAsync(http_context.HttpContext.User);
            song_entity.UserUpdate = user;

            // Add/update lyrics
            var lyrics = song_entity.Lyrics.FirstOrDefault(l => l.UserId == user.Id);
            if (lyrics == null)
            {
                lyrics = new Entities.Lyrics(user, song_entity);
                lyrics.Text = song.Lyrics;
                lyrics.UpdatedAt = DateTime.Now;
                musicdemons_context.Entry(lyrics).State = EntityState.Added;
            }
            else
            {
                lyrics.Text = song.Lyrics;
                lyrics.UpdatedAt = DateTime.Now;
                musicdemons_context.Entry(lyrics).State = EntityState.Modified;
            }

            // Update
            musicdemons_context.Entry(song_entity).State = EntityState.Modified;

            // Index
            var new_song = ToDto(song_entity);
            await elastic_client.UpdateAsync<Dtos.Song>(new_song, u => u.Doc(new_song));

            return ToDto(song_entity);
        }

        public async Task DeleteSong(int song_id)
        {
            // Find existing song
            var song = musicdemons_context.Songs.Find(song_id);

            // Get current user
            var user = await user_manager.GetUserAsync(http_context.HttpContext.User);
            song.UserDelete = user;

            // Index
            var song_dto = ToDto(song);
            await elastic_client.DeleteAsync<Dtos.Song>(song_dto);
        }

        public async Task SaveChangesAsync()
        {
            await musicdemons_context.SaveChangesAsync();
        }

        #region Conversion methods
        internal static Dtos.Song ToDto(Entities.Song song, bool include_relations = false)
        {
            if (song == null) return null;
            if (include_relations)
            {
                return new Dtos.Song
                {
                    Id = song.Id,
                    Title = song.Title,
                    Released = song.Released,
                    Lyrics = song.Lyrics.OrderBy(l => l.UpdatedAt).LastOrDefault()?.Text,
                    Artists = song.Artists.Select(@as => ArtistRepository.ToDto(@as.Artist)).ToList(),
                    Media = song.Media.Select(medium => MediumRepository.ToDto(medium, true)).ToList()
                };
            }
            else
            {
                return new Dtos.Song
                {
                    Id = song.Id,
                    Title = song.Title,
                    Released = song.Released,
                    Lyrics = song.Lyrics?.OrderBy(l => l.UpdatedAt).LastOrDefault()?.Text
                };
            }
        }
        /// <summary>Only use this method for creation of a song</summary>
        internal static Entities.Song ToEntity(Dtos.Song song, Entities.User user, MusicDemonsContext musicdemons_context)
        {
            if (song == null) return null;
            var entity_song = new Entities.Song {
                Id = song.Id,
                Title = song.Title,
                Released = song.Released
            };
            entity_song.Artists = song.Artists.Select(artist => {
                var entity_artist = musicdemons_context.Artists.Find(artist.Id);
                return new Entities.ArtistSong(entity_artist, entity_song);
            }).ToList();
            entity_song.Media = song.Media.Select(m => {
                var medium = MediumRepository.ToEntity(m, musicdemons_context);
                medium.Subject = entity_song;
                return medium;
            }).ToList();
            entity_song.Lyrics = new List<Entities.Lyrics>(new[] {
                new Entities.Lyrics { Song = entity_song, User = user, Text = song.Lyrics }
            });
            return entity_song;
        }
        #endregion
    }
}
