﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicDemons.Data.Helpers
{
    public class LoginResult
    {
        public bool Status { get; set; }
        public string Platform { get; set; }

        public Dtos.User User { get; set; }
        public string Token { get; set; }

        public string Error { get; set; }
        public string ErrorDescription { get; set; }
    }
}
