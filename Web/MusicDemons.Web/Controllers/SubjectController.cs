﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MusicDemons.Data.Repositories.Interfaces;
using MusicDemons.Web.ViewModels.Subject;

namespace MusicDemons.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SubjectController : Controller
    {
        private ISubjectRepository subjectRepository;
        public SubjectController(ISubjectRepository subjectRepository)
        {
            this.subjectRepository = subjectRepository;
        }

        [HttpGet("{subject_id}/likes")]
        public async Task<SubjectLikeVM> Likes([FromRoute]int subject_id)
        {
            var likes = await subjectRepository.GetLikes(subject_id);
            var doeslike = await subjectRepository.DoesLike(subject_id);
            return new SubjectLikeVM
            {
                Likes = likes.Item1,
                Dislikes = likes.Item2,
                Like = doeslike
            };
        }

        [Authorize]
        [HttpPost("{subject_id}/likes")]
        public async Task<SubjectLikeVM> Like([FromRoute]int subject_id, [FromBody]bool like)
        {
            await subjectRepository.Like(subject_id, like);
            await subjectRepository.SaveChangesAsync();
            var likes = await subjectRepository.GetLikes(subject_id);
            var doeslike = await subjectRepository.DoesLike(subject_id);
            return new SubjectLikeVM
            {
                Likes = likes.Item1,
                Dislikes = likes.Item2,
                Like = doeslike
            };
        }
    }
}