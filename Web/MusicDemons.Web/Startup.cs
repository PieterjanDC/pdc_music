using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using MusicDemons.Data;
using MusicDemons.Data.Extensions;
using MusicDemons.Data.Helpers;
using MusicDemons.Data.Repositories;
using MusicDemons.Data.Repositories.Interfaces;
using System;

namespace MusicDemons.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var connection_string = @"Server=(localdb)\mssqllocaldb;Database=MusicDemons;Trusted_Connection=True;ConnectRetryCount=0";
            services
                .AddDbContext<MusicDemonsContext>(options =>
                {
                    options.UseSqlServer(connection_string, b => b.MigrationsAssembly("MusicDemons.Data"));
                })
                .AddScoped<IAccountRepository, AccountRepository>()
                .AddScoped<IRoleRepository, RoleRepository>()
                .AddScoped<IPersonRepository, PersonRepository>()
                .AddScoped<IArtistRepository, ArtistRepository>()
                .AddScoped<ISubjectRepository, SubjectRepository>()
                .AddScoped<IMediumTypeRepository, MediumTypeRepository>()
                .AddScoped<IMediumRepository, MediumRepository>()
                .AddTransient<ArtistHelper>()
                .AddTransient<SongHelper>();

            services
                .AddIdentity<Data.Entities.User, Data.Entities.Role>(options =>
                {
                    options.User.RequireUniqueEmail = true;
                })
                .AddDefaultUI(Microsoft.AspNetCore.Identity.UI.UIFramework.Bootstrap4)
                .AddEntityFrameworkStores<MusicDemonsContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultForbidScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultSignOutScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.ClaimsIssuer = Configuration["JwtIssuerOptions:Issuer"];
                options.Audience = Configuration["JwtIssuerOptions:Audience"];
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["JwtIssuerOptions:Issuer"],

                    ValidateAudience = true,
                    ValidAudience = Configuration["JwtIssuerOptions:Audience"],

                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new Func<SymmetricSecurityKey>(() =>
                    {
                        var key = Configuration["JwtIssuerOptions:Key"];
                        var bytes = System.Text.Encoding.UTF8.GetBytes(key);
                        var signing_key = new SymmetricSecurityKey(bytes);
                        return signing_key;
                    }).Invoke(),

                    ValidateLifetime = true,
                };
                options.SaveToken = false;
                options.RequireHttpsMetadata = false;
            });

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDataProtection();
            services.AddElasticSearch(Configuration);
            
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            services.Configure<ForwardedHeadersOptions>(options => {
                options.ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto;
            }).Configure<IdentityOptions>(options => {

                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = System.TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;
                options.User.AllowedUserNameCharacters = string.Empty;
            }).Configure<JwtIssuerOptions>(options => {
                options.Issuer = Configuration["JwtIssuerOptions:Issuer"];
                options.Subject = Configuration["JwtIssuerOptions:Subject"];
                options.Audience = Configuration["JwtIssuerOptions:Audience"];
                options.ValidFor = Configuration.GetValue<TimeSpan>("JwtIssuerOptions:ValidFor");
                options.Key = Configuration["JwtIssuerOptions:Key"];
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app
                .UseHsts()
                .UseHttpsRedirection()
                .UseForwardedHeaders()
                .UseStaticFiles()
                .UseSpaStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}"
                );
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";
                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}