import { MediumTypeModule } from './medium-type.module';

describe('MediumTypeModule', () => {
  let mediumTypeModule: MediumTypeModule;

  beforeEach(() => {
    mediumTypeModule = new MediumTypeModule();
  });

  it('should create an instance', () => {
    expect(mediumTypeModule).toBeTruthy();
  });
});
