import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AccountService } from 'src/app/services/account/account.service';
import { Router } from '@angular/router';
import { LoginResult } from 'src/app/interfaces/loginResult';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from 'src/app/entities/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private accountService: AccountService, private router: Router) {
  }

  email: string;
  password: string;
  loginResult: LoginResult = {
    status: true,
    platform: 'local',
    user: null,
    token: null,
    error: null,
    errorDescription: null
  };

  login() {
    this.accountService.login(this.email, this.password).subscribe((result) => {
      if (result.status === true) {
        localStorage.setItem('auth_token', result.token);
        this.router.navigate(['/']);
        this.loginComplete.emit(result.user);
      } else {
        this.loginResult = result;
      }
    }, (error: HttpErrorResponse) => {
      this.loginResult = {
        status: false,
        platform: 'local',
        user: null,
        token: null,
        error: 'Login attempt failed',
        errorDescription: 'Check your connection'
      };
    });
  }

  @Output() loginComplete: EventEmitter<User> = new EventEmitter();

  ngOnInit() {
  }

}
