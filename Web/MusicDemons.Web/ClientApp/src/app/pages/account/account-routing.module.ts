import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { IsLoggedInGuard } from '../../guards/IsLoggedIn/is-logged-in.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent, data: { animation: 'login' }  },
  { path: 'register', component: RegisterComponent, data: { animation: 'register' }  },
  { path: 'profile', component: ProfileComponent, data: { animation: 'profile' } , canActivate: [IsLoggedInGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
