import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { ShowComponent } from './show/show.component';
import { IsLoggedInGuard } from '../../guards/IsLoggedIn/is-logged-in.guard';

const routes: Routes = [
  { path: '', component: ListComponent, data: { animation: 'artists' } },
  { path: 'create', component: CreateComponent, data: { animation: 'artist-create' } , canActivate: [IsLoggedInGuard] },
  { path: ':id/edit', component: EditComponent, data: { animation: 'artist-edit' } , canActivate: [IsLoggedInGuard] },
  { path: ':id', component: ShowComponent, data: { animation: 'artist' }  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistRoutingModule { }
