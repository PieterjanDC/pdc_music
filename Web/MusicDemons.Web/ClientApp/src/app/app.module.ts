import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, ROUTES } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ControlsModule } from './controls/controls.module';
import { ComponentsModule } from './components/components.module';
import { BearerTokenInterceptor } from './interceptors/bearerTokenInterceptor';
import { IsLoggedInGuard } from './guards/IsLoggedIn/is-logged-in.guard';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    ControlsModule,
    ComponentsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: BearerTokenInterceptor,
    multi: true
  //}, {
  //    provide: ,
  //  useClass: IsLoggedInGuard,
  //  multi: true
    },
    IsLoggedInGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
