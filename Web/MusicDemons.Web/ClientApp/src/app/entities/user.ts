export interface User {
	id: string;
	userName: string;
	email: string;
	pictureUrl: string;
}
