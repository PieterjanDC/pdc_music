import { User } from "../entities/user";

export interface UserData {
  user: User;
  password: string;
  passwordConfirmation: string;
}
