import { User } from "../entities/user";

export interface LoginResult {
  status: boolean;
  platform: string;
  user: User;
  token: string;
  error: string;
  errorDescription: string;
}
