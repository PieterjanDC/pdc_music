import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SlideUpDownAnimation } from '../../animations/slide';
import { User } from 'src/app/entities/user';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    SlideUpDownAnimation
  ]
})
export class SidebarComponent implements OnInit {

  constructor() {
  }

  level1menu: string = "";
  level2menu: string = "";
  level1toggle(menu: string) {
    if (this.level1menu === menu) {
      this.level1menu = "";
    } else {
      this.level1menu = menu;
    }
  }

  public logout() {
    this.logoutClicked.emit(null);
  }

  @Input() activeUser: User;
  @Output() logoutClicked: EventEmitter<string> = new EventEmitter();

  ngOnInit() {
  }

}
