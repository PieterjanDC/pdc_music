import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarTogglerComponent } from './navbar-toggler/navbar-toggler.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NavbarTogglerComponent],
  exports: [NavbarTogglerComponent]
})
export class ControlsModule { }
