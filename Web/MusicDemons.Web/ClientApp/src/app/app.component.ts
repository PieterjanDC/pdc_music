import { Component, ElementRef } from '@angular/core';
import { eToggleButtonState } from './enums/eToggleButtonState';
import { eSidebarState } from './enums/eSidebarState';
import { User } from './entities/user';
import { LoginComponent } from './pages/account/login/login.component';
import { RegisterComponent } from './pages/account/register/register.component';
import { AccountService } from './services/account/account.service';
import { slideInAnimation } from './animations/slideIn';
import { RouterOutlet } from '@angular/router';
import { fadeAnimation } from './animations/fade.animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    slideInAnimation,
    fadeAnimation
  ]
})
export class AppComponent {
  title = 'app';
  toggleButtonState: eToggleButtonState = eToggleButtonState.auto;
  sidebarState: eSidebarState = eSidebarState.auto;
  activeUser: User = null;

  constructor(private accountService: AccountService) {
    this.accountService.currentUser().subscribe((user) => {
      this.activeUser = user;
    }, (error) => {
      this.activeUser = null;
    });
  }

  updateSidebarState(state: eToggleButtonState) {
    switch (state) {
      case eToggleButtonState.open:
        this.sidebarState = eSidebarState.show;
        break;
      case eToggleButtonState.closed:
        this.sidebarState = eSidebarState.hide;
        break;
      default:
        this.sidebarState = eSidebarState.auto;
        break;
    }
  }

  loginCompleted = (user: User) => {
    this.activeUser = user;
  }

  logoutClicked() {
    this.accountService.logout().subscribe(() => {
      this.activeUser = null;
      localStorage.removeItem('auth_token');
    });
  }

  routingActivated(element: ElementRef) {
    // Login complete
    if (element instanceof LoginComponent) {
      element.loginComplete.subscribe(this.loginCompleted);
    } else if (element instanceof RegisterComponent) {
      element.loginComplete.subscribe(this.loginCompleted);
    }
  }

  routingDeactivated(element: ElementRef) {
    // Login complete
    if (element instanceof LoginComponent) {
      element.loginComplete.unsubscribe();
    } else if (element instanceof RegisterComponent) {
      element.loginComplete.unsubscribe();
    }
  }

  private counter: number = 0;
  public getAnimationData(outlet) {
    //return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
    //return ++this.counter;
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
