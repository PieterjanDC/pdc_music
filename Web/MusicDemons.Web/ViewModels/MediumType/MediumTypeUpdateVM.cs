﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicDemons.Web.ViewModels.MediumType
{
    public class MediumTypeUpdateVM
    {
        public Data.Dtos.MediumType MediumType { get; set; }
    }
}
