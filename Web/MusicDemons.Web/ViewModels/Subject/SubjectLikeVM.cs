﻿namespace MusicDemons.Web.ViewModels.Subject
{
    public class SubjectLikeVM
    {
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public bool? Like { get; set; }
    }
}
