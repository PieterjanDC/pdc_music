﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicDemons.Web.ViewModels.Artist
{
    public class ArtistUpdateVM
    {
        public Data.Dtos.Artist Artist { get; set; }
    }
}
