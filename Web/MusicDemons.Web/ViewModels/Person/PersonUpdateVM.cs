﻿namespace MusicDemons.Web.ViewModels.Person
{
    public class PersonUpdateVM
    {
        public Data.Dtos.Person Person { get; set; }
    }
}
