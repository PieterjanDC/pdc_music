﻿namespace MusicDemons.Web.ViewModels.Song
{
    public class SongCreateVM
    {
        public Data.Dtos.Song Song { get; set; }
    }
}
